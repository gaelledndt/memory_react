import {useEffect, useState} from 'react';
import fruitItems from './fruits.json';
import './App.css';

/**
 * Fonction qui permet de sélectionner une carte
 * @param fruit
 * @param flipped
 * @param chooseCard
 * @returns {JSX.Element}
 * @constructor
 */
function Card({ fruit, flipped, chooseCard }) {
    const [clicked, setClicked] = useState(false);

    const cardClickHandle = () => {
        if (!flipped && !clicked) {
            chooseCard(fruit);
            setClicked(true);
        }
    };

    useEffect(() => {
        if (flipped) {
            const timer = setTimeout(() => {
                setClicked(false);
            }, 1000);
            return () => clearTimeout(timer);
        }
    }, [flipped]);

    return (
        <div
            className={`card ${flipped || clicked ? 'matched' : ''}`}
            onClick={cardClickHandle}
        >
            {flipped || clicked ? (
                <img style={{ transform: 'rotateY(180deg)' }} src={fruit.src} alt={fruit.name} />
            ) : (
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="40"
                    height="40"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                >
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M8 8a3.5 3 0 0 1 3.5 -3h1a3.5 3 0 0 1 3.5 3a3 3 0 0 1 -2 3a3 4 0 0 0 -2 4" />
                    <line x1="12" y1="19" x2="12" y2="19.01" />
                </svg>
            )}
        </div>
    );
}


//le rotateY à 180deo permet de faire retourner l'image

/**
 * Fonction principale
 * @returns {JSX.Element}
 * @constructor
 */
function App() {
    const [fruits, setFruits] = useState([]);
    const [selectedCards, setSelectedCards] = useState([]);

    const chooseCard = (fruit) => {
        if (selectedCards.length < 2 && !fruit.matched && !selectedCards.includes(fruit)) {
            setSelectedCards((prevSelectedCards) => [...prevSelectedCards, fruit]);
        }
    };

    useEffect(() => {
        if (selectedCards.length === 2) {
            const [card1, card2] = selectedCards;
            if (card1.src === card2.src) {
                setFruits((prevFruits) =>
                    prevFruits.map((item) =>
                        item.src === card1.src ? { ...item, matched: true } : item
                    )
                );
            }
            setTimeout(() => {
                setSelectedCards([]);
            }, 1000);
        }
    }, [selectedCards]);

    const initGame = () => {
        const allFruits = [...fruitItems, ...fruitItems]
            .sort(() => Math.random() - 0.5)
            .map((item) => ({ ...item, id: Math.random() }));
        setFruits(allFruits);
        setSelectedCards([]);
    };

    const resetGame = () => {
        setFruits((prevFruits) =>
            prevFruits.map((item) => (item.matched ? { ...item, matched: false } : item))
        );
        setSelectedCards([]);
        setTimeout(() => {
            initGame();
        }, 500);
    };

    return (
        <>
            <h1>Memory</h1>
            {fruits.length ? (
                <>
                    <button className="reset" onClick={resetGame}>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="40"
                            height="40"
                            viewBox="0 0 24 24"
                            strokeWidth="1"
                            stroke="currentColor"
                            fill="none"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        >
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M19.933 13.041a8 8 0 1 1 -9.925 -8.788c3.899 -1.002 7.935 1.007 9.425 4.747"></path>
                            <path d="M20 4v5h-5"></path>
                        </svg>
                    </button>
                    <div className="game-block">
                        {fruits.map((fruit, key) => (
                            <Card
                                key={key}
                                chooseCard={chooseCard}
                                flipped={selectedCards.includes(fruit) || fruit.matched}
                                fruit={fruit}
                            />
                        ))}
                    </div>
                </>
            ) : (
                <button className="start-game" onClick={initGame}>
                    Commencer !!
                </button>
            )}
        </>
    );
}


export default App;